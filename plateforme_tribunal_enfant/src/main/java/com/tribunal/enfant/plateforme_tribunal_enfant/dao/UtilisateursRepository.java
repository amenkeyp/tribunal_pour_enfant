package com.tribunal.enfant.plateforme_tribunal_enfant.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tribunal.enfant.plateforme_tribunal_enfant.beans.Utilisateur;
@Repository
public interface UtilisateursRepository extends JpaRepository<Utilisateur, Integer> {

}
