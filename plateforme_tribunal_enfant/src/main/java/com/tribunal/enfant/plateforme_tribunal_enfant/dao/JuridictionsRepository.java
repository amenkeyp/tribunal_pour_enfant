package com.tribunal.enfant.plateforme_tribunal_enfant.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tribunal.enfant.plateforme_tribunal_enfant.beans.Juridiction;

public interface JuridictionsRepository extends JpaRepository<Juridiction, Integer>{

}
