package com.tribunal.enfant.plateforme_tribunal_enfant.Resource;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.tribunal.enfant.plateforme_tribunal_enfant.beans.Juridiction;
import com.tribunal.enfant.plateforme_tribunal_enfant.dao.JuridictionsRepository;

public class JuridictionResources  {
	@Autowired
	private JuridictionsRepository juridictionRepository;
	
	@PostMapping("/createjuridiction")
	public Juridiction createJuridiction(@Valid @RequestBody Juridiction juridiction) {
		return juridictionRepository.save(juridiction);
		
	}
	@GetMapping("/Alljuridictions")
	
	public Page<Juridiction> getAlljuridiction(Pageable pageable){
		return juridictionRepository.findAll(pageable);
		
		
	}
	
	@GetMapping("/FindOne")
	public Juridiction FindOne(Integer id) {
		return juridictionRepository.findById(id).get();
	}

}
