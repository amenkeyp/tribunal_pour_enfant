package com.tribunal.enfant.plateforme_tribunal_enfant.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tribunal.enfant.plateforme_tribunal_enfant.beans.Profil;

@Repository
public interface ProfilsRepository  extends JpaRepository<Profil, Integer>{

}
