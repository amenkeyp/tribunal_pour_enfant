package com.tribunal.enfant.plateforme_tribunal_enfant.beans;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="juridictions")
public class Juridiction extends AuditModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id 
	@GeneratedValue
	private Integer id;
    private String libelle;
	private String description;
	private String Lieu;
	private String juridiction_code;
	private String codeenr;
	private String type;
	

	public Juridiction() {
		super();
		
	}

	public Juridiction(Integer id, String libelle, String description, String lieu, String juridiction_code,
			String codeenr, String type) {
		super();
		this.id = id;
		this.libelle = libelle;
		this.description = description;
		Lieu = lieu;
		this.juridiction_code = juridiction_code;
		this.codeenr = codeenr;
		this.type = type;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLieu() {
		return Lieu;
	}

	public void setLieu(String lieu) {
		Lieu = lieu;
	}

	public String getJuridiction_code() {
		return juridiction_code;
	}

	public void setJuridiction_code(String juridiction_code) {
		this.juridiction_code = juridiction_code;
	}

	public String getCodeenr() {
		return codeenr;
	}

	public void setCodeenr(String codeenr) {
		this.codeenr = codeenr;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Juridiction [id=" + id + ", libelle=" + libelle + ", description=" + description + ", Lieu=" + Lieu
				+ ", juridiction_code=" + juridiction_code + ", codeenr=" + codeenr + ", type=" + type + "]";
	}
	
	
	
	

}
