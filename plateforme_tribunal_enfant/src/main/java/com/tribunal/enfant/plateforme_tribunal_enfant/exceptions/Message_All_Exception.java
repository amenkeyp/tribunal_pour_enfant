package com.tribunal.enfant.plateforme_tribunal_enfant.exceptions;

import java.util.Date;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.tribunal.enfant.plateforme_tribunal_enfant.beans.Message_Exception_Entity;
@ControllerAdvice
@RestController
public class Message_All_Exception extends ResponseEntityExceptionHandler{
	
	@ExceptionHandler(Message_Exception.class)
	public final ResponseEntity<Object> statusNotFoundhandleException(Message_Exception ex, WebRequest request) {
		Message_Exception_Entity exceptionreponse = new Message_Exception_Entity(new Date(),
				ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<Object>(exceptionreponse,HttpStatus.NOT_FOUND);
		
	}

}
