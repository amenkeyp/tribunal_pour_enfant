package com.tribunal.enfant.plateforme_tribunal_enfant.controllers;

import java.util.Date;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tribunal.enfant.plateforme_tribunal_enfant.Resource.ProfilResources;
import com.tribunal.enfant.plateforme_tribunal_enfant.Resource.UtilisateurResources;
import com.tribunal.enfant.plateforme_tribunal_enfant.beans.Profil;
import com.tribunal.enfant.plateforme_tribunal_enfant.beans.Utilisateur;

@Controller
public class ControllerPage {
	
	@Autowired
	private ProfilResources profilResource;
	@Autowired
	private UtilisateurResources utilisateurResource;
	
	@GetMapping("/index")
	public String index() {
		return "index";
	}
	
	
	@GetMapping("/pricing_tables")
	public String pricing() {
		return "pricing_tables";
	}
	
	@GetMapping("/profile")
	public String profil() {
		return "profile";
	}
	
	@GetMapping("/plain_page")
	public String plain_page() {
		return "plain_page";
	}
	
	@GetMapping("/login")
	public String login() {
		return "login";
	}
	
	
	@GetMapping("/form_validation")
	public String formvalidation() {
		return "form_validation";
	}
	@GetMapping("/form_advanced")
	public String form_advanced() {
		return "form_advanced";
	}
	@GetMapping("/form_upload")
	public String form_upload() {
		return "form_upload";
	}
	@GetMapping("/form_buttons")
	public String form_buttons() {
		return "form_buttons";
	}
	@GetMapping("/form_wizards")
	public String form_wizards() {
		return "form_wizards";
	}
	
	@GetMapping("/Compte")
	public String Compte() {
		return "Compte";
	}
	@GetMapping("/calendar")
	public String calendar() {
		return "calendar";
	}

	@GetMapping("/fixed_sidebar")
	public String fixed_sidebar() {
		return "fixed_sidebar";
	}
	@GetMapping("/fixed_footer")
	public String fixed_footer() {
		return "fixed_footer";
	}
	
	@GetMapping("/tables")
	public String tables(){
		return "tables";
	}
	
	@GetMapping("/projects")
	public String projects() {
		return "projects";
	}
	
	@GetMapping("/project_detail")
	public String project_detail() {
		return "project_detail";
	}
	
	@GetMapping("/invoice")
	public String invoice() {
		return "invoice";
	}
	@GetMapping("/map")
	public String map() {
		return "map";
	}
	
	@GetMapping("/adduser")
	public String add(Model model,Pageable pageable) {
		model.addAttribute("utilisateur", new Utilisateur());
		return "admin/user/adduser";
		
	}
	
 @GetMapping("admin/user/listes_users")
	public String addF(Model model, Pageable pageable ) {
		System.out.println("pppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppp");
	  model.addAttribute("users", utilisateurResource.getAllusers(pageable));
	  System.out.println("000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000");
		return "admin/user/listes_users";
}
	
@PostMapping("admin/user/listes_users")
	public String form(@Valid @ModelAttribute Utilisateur util, BindingResult result, Model model,Pageable pageable) {
		util.setCreated_at(new Date());
		util.setUpdated_at(new Date());
		utilisateurResource.createUsers(util);
		model.addAttribute("users", utilisateurResource.getAllusers(pageable));
		return "admin/user/listes_users";
		
	}
	
	@GetMapping("/listeprofil")
	public String listeProfilTables(Model model , @RequestParam (defaultValue = "0") int page) {
		model.addAttribute("profils", profilResource.getAllProfils(new PageRequest(page, 4)));
		model.addAttribute("current", page);
		return "admin/profil/listes_profils";
	}
	
	/*Profil*/
	@GetMapping("/findOneProfil")
	@ResponseBody
	public Profil FindOneProfilId(Integer id) {
		return profilResource.FindOne(id);		
	}
	
	@GetMapping("/addprofil")
	public String form(Model model, Pageable pageable) {
	model.addAttribute("profil" , new Profil());
		return "admin/profil/addprofil";
	}
	
	@PostMapping("/addProfil")
	public String addProfil (@Valid @ModelAttribute Profil profil, BindingResult result, Model model, Pageable pageable) {
		profil.setCreated_at(new Date()); 
		profil.setUpdated_at(new Date());
		profilResource.CreateProfils(profil);
		model.addAttribute("profils",profilResource.getAllProfils(pageable));
		return "redirect:/listeprofil";
	}	
@PostMapping("/updateProfil")
public String updateProfil(Integer id,@Valid Profil profil,BindingResult result, Model model, Pageable pageable ) {
	profil.setCreated_at(new Date()); 
	profil.setUpdated_at(new Date());
	if(result.hasErrors()) {
		profil.setId(id);
		return "redirect:/listeprofil";
	}
		profilResource.updateProfilById(profil.getId(), profil);
		model.addAttribute("profils",profilResource.getAllProfils(pageable));
		return "redirect:/listeprofil";	
}

@PostMapping("/deleted")	
public String deleteProfilbyId(Integer id, Model model, @RequestParam(defaultValue = "0")int page) {
	profilResource.deleteProfilById(id);
	return "redirect:/listeprofil";
}

}
