package com.tribunal.enfant.plateforme_tribunal_enfant.beans;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="users")
public class Utilisateur extends AuditModel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private Integer id;
	private String login;
	private String Password;
	private String nom;
	private String prenom;
	private String mail;
	private String corps;
	private String juridiction_code;
	private String photo;
	private String Civilite;
	
	
	
	
	
	
	public Utilisateur() {
		super();
	}

	

	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getLogin() {
		return login;
	}


	public void setLogin(String login) {
		this.login = login;
	}


	public String getPassword() {
		return Password;
	}


	public void setPassword(String password) {
		Password = password;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getPrenom() {
		return prenom;
	}


	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}


	public String getMail() {
		return mail;
	}


	public void setMail(String mail) {
		this.mail = mail;
	}


	public String getCorps() {
		return corps;
	}


	public void setCorps(String corps) {
		this.corps = corps;
	}


	public String getJuridiction_code() {
		return juridiction_code;
	}


	public void setJuridiction_code(String juridiction_code) {
		this.juridiction_code = juridiction_code;
	}


	public String getPhoto() {
		return photo;
	}


	public void setPhoto(String photo) {
		this.photo = photo;
	}


	public String getCivilite() {
		return Civilite;
	}


	public void setCivilite(String civilite) {
		Civilite = civilite;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public Utilisateur(Integer id, String login, String password, String nom, String prenom, String mail, String corps,
			String juridiction_code, String photo, String civilite) {
		super();
		this.id = id;
		this.login = login;
		Password = password;
		this.nom = nom;
		this.prenom = prenom;
		this.mail = mail;
		this.corps = corps;
		this.juridiction_code = juridiction_code;
		this.photo = photo;
		Civilite = civilite;
	}



	@Override
	public String toString() {
		return "Utilisateur [id=" + id + ", login=" + login + ", Password=" + Password + ", nom=" + nom + ", prenom="
				+ prenom + ", mail=" + mail + ", corps=" + corps + ", juridiction_code=" + juridiction_code + ", photo="
				+ photo + ", Civilite=" + Civilite + "]";
	}



	

	
}
